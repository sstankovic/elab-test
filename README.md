# WolkAbout Embedded Laboratory Developer Test
---

## Prerequisite
 - Account created on [WolkAbout IoT Demo Platform](https://demo.wolkabout.com/#/get-started)
 - [WolkConnect-C](https://github.com/Wolkabout/WolkConnect-C) or [WolkConnect-Cpp](https://github.com/Wolkabout/WolkConnect-Cpp)
 - Linux OS

## Task
Develop a standalone Linux App called *ELab-Test*. *ELab-Test* app is a computer monitoring tool which is used for visualizing data on WolkAbout IoT Platform.
It needs to be implemented as a console app and runnable on any Linux machine. Develop app in C or Cpp programming language and in according with chosen language use one of the WolkConnect libraries listed in the prerequisite section.

### 1 Create device
Create device on WolkAbout Platform using given *ELab-Test-manifest.json* manifest.

### 2 Monitoring IP address
Implement monitoring of current IP address at every 5 min. Publish it to the platform only if it's changed from initial address.

Note: "IP_ADD" is IP address reference on WolkAbout IoT Demo Platform.

### 3 Monitoring CPU temperature
Implement monitoring CPU temperature every minute. Publish the maximum value within a 5 minute timeframe to the platform.

Note: "CPU_T" is temperature reference.

### 4 Logging
Log all activities both in-app and to file while running. Configuring logging level (INFO and DEBUG) should be able to be changed from WolkAbout IoT Platform by editing device's configuration options.

Note: "LOG_LEVEL" is logging level reference.

## Deploying Task
Fork from this repo, develop task and create a Pull Request when you are done.

**NOTE:** make your output runable